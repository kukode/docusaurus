---
sidebar_position: 1
---

# SKB - CAT

Docs: https://docs.google.com/document/d/1JWAC2Te5JML3iICEnnPa7EyeBnuz8LsqSlF39IMpMeg

## Kemampuan Umum

### 1. UU Nomor 5 Tahun 2014 tentang Aparatur Sipil Negara



### 2. PP Nomor 11 Tahun 2017 tentang Manajemen Pegawai Negeri Sipil



### 3. PP Nomor 17 Tahun 2020 tentang Perubahan atas PP Nomor 11 Tahun 2017



### 4. PP Nomor 49 Tahun 2018: Manajemen Pegawai Pemerintah dengan Perjanjian Kerja (PPPK)



### 5. Permen PANRB Nomor 1 Tahun 2023 tentang Jabatan Fungsional



## Kemampuan Khusus:

### 1. Jabatan Fungsional Analis SDM Aparatur

Jabatan Fungsional Analis SDM Aparatur adalah jabatan yang memiliki tugas pokok melaksanakan analisis di bidang sumber daya manusia aparatur untuk mendukung pengelolaan ASN yang lebih profesional, efektif, dan efisien.

1. Kategori Jabatan

Jabatan Fungsional Tertentu, bersifat keahlian (bukan keterampilan).
Memiliki jenjang karir dari:

- Jenjang Ahli Pertama,
- Jenjang Ahli Muda,
- Jenjang Ahli Madya,
- Jenjang Ahli Utama.

:::tip Baca

Use this awesome [feature option](https://google.com)

:::

```
https://google.com
```

2. Tugas Pokok dan Fungsi Analis SDM Aparatur

Tugas utama Analis SDM Aparatur adalah melakukan analisis yang meliputi:
- Perencanaan SDM Aparatur:
  Melakukan analisis kebutuhan SDM untuk mendukung pencapaian tujuan organisasi.
- Pengembangan Kompetensi:
  Menyusun program pengembangan kompetensi ASN melalui pelatihan, pendidikan, dan pengembangan lainnya.
- Pengelolaan Kinerja:
  Menganalisis dan mengevaluasi pelaksanaan sistem kinerja ASN, termasuk penyusunan Sasaran Kinerja Pegawai (SKP).
- Manajemen Karir dan Promosi:
  Melakukan analisis jalur karier, pengisian jabatan, dan pola promosi ASN.
- Penyusunan Sistem Informasi SDM:
  Mengembangkan sistem informasi yang mendukung pengelolaan SDM berbasis teknologi.
- Evaluasi dan Perbaikan Kebijakan SDM:
  Memberikan rekomendasi berdasarkan hasil evaluasi kebijakan terkait ASN.

3. Kompetensi yang Dibutuhkan

Kompetensi Teknis:
Pemahaman mendalam tentang manajemen ASN dan regulasi terkait, seperti:
UU No. 5 Tahun 2014 tentang ASN.
PP No. 11 Tahun 2017 dan PP No. 17 Tahun 2020 tentang Manajemen ASN.
Kemampuan analisis data SDM berbasis sistem informasi.
Kompetensi Manajerial:
Kemampuan mengelola waktu, komunikasi, dan pengambilan keputusan.
Kompetensi Sosial Kultural:
Mampu memahami budaya kerja organisasi pemerintah dan memberikan rekomendasi berdasarkan nilai-nilai kebangsaan.

4. Jenjang Jabatan

- Ahli Pertama: Berfokus pada pelaksanaan tugas teknis analisis dasar.
- Ahli Muda: Melaksanakan analisis yang lebih kompleks dan mendukung pengambilan keputusan organisasi.
- Ahli Madya: Mengelola tim analisis, memberikan supervisi, dan menyusun kebijakan strategis.
- Ahli Utama: Memberikan arahan kebijakan tingkat nasional, menjadi konsultan ahli dalam perencanaan SDM.

### 2. Penyusunan dan Penetapan Kebutuhan ASN



### 3. Pengadaan ASN



### 4. Pangkat dan Jabatan ASN



### 5. Pengembangan Karier ASN



### 6. Pola Karier ASN



### 7. Promosi ASN



### 8. Mutasi ASN



### 9. Penugasan ASN



### 10. Pengembangan Kompetensi ASN



### 11. Penilaian Kinerja ASN



### 12. Disiplin ASN



### 13. Penghargaan ASN



### 14. Penggajian, Tunjangan, dan Fasilitas ASN



### 15. Pemberhentian ASN



### 16. Jaminan Pensiun dan Jaminan Hari Tua ASN



### 17. Perlindungan ASN



### 18. Cuti ASN



### 19. Sistem Informasi ASN



### 20. Talenta/reformasi birokrasi/zona integritas



### 21. Struktur/kelembagaan/tata laksana/ proses bisnis unit kerja/instansi



### 22. Kelembagaan ASN dan/atau lembaga pengelola kepegawaian dengan unit kerja dalam penguatan efektivitas organisasi



### 23. Proses penyusunan kebijakan/regulasi bidang SDM Aparatur